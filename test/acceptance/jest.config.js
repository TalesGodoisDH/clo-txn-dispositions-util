module.exports = {
  globals: {
    'ts-jest': {
      diagnostics: true,
      tsconfig: 'tsconfig.json',
    },
  },
  transform: {
    '^.+\\.ts?$': 'ts-jest',
  },
  rootDir: '.',
  testMatch: ['**/*.test.(ts|js)'],
  moduleFileExtensions: ['ts', 'js'],
  testTimeout: 12000,
};
